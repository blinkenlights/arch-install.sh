# arch-install.sh

Simple, opinionated installation script for Blinkenlights workstations.


## Usage

```sh
curl -sL https://arch.blinkenlights.se/install.sh | bash
```

The script _will_ wipe disks without warning.
