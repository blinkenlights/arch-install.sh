#!/bin/bash

set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

function ask_user_info () {
    read -p "Hostname: " hostname
    : ${hostname:?"hostname cannot be empty"}

    read -p "Username: " user
    : ${user:?"user cannot be empty"}

    read -sp "Password: " password
    : ${password:?"password cannot be empty"}

    read -sp "Repeat password: " password2
    [[ "$password" == "$password2" ]] || ( echo "Passwords did not match"; exit 1; )

    read -sp "LUKS passphrase: " passwordluks
    : ${passwordluks:?"LUKS passphrases cannot be empty"}

    read -sp "Repeat LUKS passphrase: " passwordluks2
    [[ "$passwordluks" == "$passwordluks2" ]] || ( echo "LUKS passphrases did not match"; exit 1; )

    select device_root in $(lsblk -dplnx size -o name | grep -Ev "boot|rpmb|loop" | tac)
    do
      break
    done

    clear
}

function setup_disks () {
    # wipe disks
    wipefs -f -a $device_root

    # partition
    parted --script -a optimal "${device_root}" -- \
        mklabel gpt \
        mkpart ESP fat32 1MiB 1GiB \
        mkpart primary ext4 1GiB 100% \
        set 1 boot on

    sleep 1

    echo "partprobe"
    partprobe

    sleep 1

    part_boot="$(ls ${device_root}* | grep -E "^${device_root}p?1$")"
    part_root="$(ls ${device_root}* | grep -E "^${device_root}p?2$")"

    echo "boot=${part_boot}"
    echo "root=${part_root}"

    # format boot partition
    mkfs.vfat -F32 "${part_boot}"

    # set up luks
    luks_device="luks_${hostname}"

    echo -n "$passwordluks" | cryptsetup luksFormat ${part_root} -
    echo -n "$passwordluks" | cryptsetup luksOpen   ${part_root} $luks_device -

    # format luks device
    mkfs.ext4 "/dev/mapper/${luks_device}"
}

function mount_disks () {
    mount "/dev/mapper/${luks_device}" /mnt
    mount --mkdir "${part_boot}" /mnt/boot
}

function install_base_packages () {
    cat >>/etc/pacman.conf <<EOF
[blinkenlights]
SigLevel = Never
Server = https://arch.blinkenlights.se/repo/x86_64
EOF

    pacstrap /mnt $hostname-desktop
}

function setup_base_system () {
    genfstab -t PARTUUID /mnt >> /mnt/etc/fstab
    echo "${hostname}" > /mnt/etc/hostname

    arch-chroot /mnt useradd -mU -s /usr/bin/zsh -G wheel,uucp,video,audio,storage,games,input "$user"
    arch-chroot /mnt chsh -s /usr/bin/zsh

    echo "$user:$password" | chpasswd --root /mnt
    echo "root:$password" | chpasswd --root /mnt

    arch-chroot /mnt bootctl install

    cat <<EOF > /mnt/boot/loader/loader.conf
default arch
EOF

    cat <<EOF > /mnt/boot/loader/entries/arch.conf
title   Arch
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options ipv6.disabled=1 rd.luks.name=$(blkid -s UUID -o value "$part_root")=$luks_device root=/dev/mapper/$luks_device rw
EOF

   cat >>/mnt/etc/pacman.conf <<EOF
[blinkenlights]
SigLevel = Never
Server = https://arch.blinkenlights.se/repo/x86_64
EOF

    ln -sf ../run/systemd/resolve/stub-resolv.conf /mnt/etc/resolv.conf

    sed -i "s/^HOOKS=.*/HOOKS=(base autodetect modconf keyboard block systemd sd-vconsole sd-encrypt filesystems fsck)/g" /mnt/etc/mkinitcpio.conf

    arch-chroot /mnt mkinitcpio -P

    # set up dhcp networking
    cat <<EOF > /mnt/etc/systemd/network/10-wired.network
[Match]
Name=enp*

[Network]
DHCP=yes
EOF

    arch-chroot /mnt systemctl enable lightdm
    arch-chroot /mnt systemctl enable systemd-networkd
    arch-chroot /mnt systemctl enable systemd-resolved
}

function unmount_disks () {
    umount -R /mnt
    cryptsetup luksClose /dev/mapper/$luks_device
}

function install () {
    ask_user_info

    ### Set up logging ###
    exec 1> >(tee "stdout.log")
    exec 2> >(tee "stderr.log")

    timedatectl set-ntp true

    setup_disks
    mount_disks
    install_base_packages
    setup_base_system
    unmount_disks

    echo "Arch installed. Reboot now."
}

install
